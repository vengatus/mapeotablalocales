

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper


class BackEndManager {
    func getLocales(lat:Double,long:Double,radio:Int=10){
        let url = "https://turismo-webservice-vengatus.c9users.io/company/getnearcompanies/?lat=\(lat)&long=\(long)&radio=\(radio)"
        Alamofire.request(url).responseArray { (response: DataResponse<[Local]>) in
            
            if let localesArr = response.result.value {
                locales = localesArr
                NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object: nil)
            }
        }
    }
    
    func getLocalImage(_ id:Int,_ numImg:Int,completionHandler: @escaping(UIImage)->()){
            let url = "https://turismo-webservice-vengatus.c9users.io/images/companies/\(id)/\(numImg).png"
            Alamofire.request(url).responseImage { response in
                if let image = response.result.value {
                    completionHandler(image)
                }
            }
        
    }
}
