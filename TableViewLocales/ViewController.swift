//
//  ViewController.swift
//  TableViewLocales
//
//  Created by Juan Erazo on 8/12/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    let bm = BackEndManager()
    var locationManager = CLLocationManager()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        let location = locationManager.location?.coordinate
        
        NotificationCenter.default.addObserver(self, selector:#selector(actualizar), name: NSNotification.Name("actualizar"), object: nil)
        	
        bm.getLocales(lat: (location?.latitude)!, long: (location?.longitude)!, radio:40)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func actualizar(){
        tableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locales.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let localCell = tableView.dequeueReusableCell(withIdentifier: "localCell", for: indexPath) as! LocalTableViewCell
        localCell.fillData(locales[indexPath.row])
        return localCell
    }

}

