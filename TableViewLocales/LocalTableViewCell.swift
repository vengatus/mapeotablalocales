//
//  LocalTableViewCell.swift
//  TableViewLocales
//
//  Created by Juan Erazo on 8/12/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit
import ObjectMapper

class LocalTableViewCell: UITableViewCell {
    @IBOutlet weak var localImage: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var distanciaLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fillData(_ local:Local){
        self.tituloLabel.text = local.name!
        self.distanciaLabel.text = local.distanceText!
        if local.imagenes.count == 0 {
            let bm =  BackEndManager()
            bm.getLocalImage(local.id!, 0, completionHandler: { (img) in
                local.imagenes.append(img)
                self.localImage.image = local.imagenes[0]
            })
            
        }
    }

}
