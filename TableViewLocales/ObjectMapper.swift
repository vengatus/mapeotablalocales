//
//  ObjectMapper.swift
//  TableViewLocales
//
//  Created by Juan Erazo on 8/12/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import Foundation
import ObjectMapper

class Local:Mappable {
    var id:Int?
    var name:String?
    var description:String?
    var latitude:Double?
    var longitude:Double?
    var totalImages:Int?
    var distanceText:String?
    var imagenes = [UIImage]()
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        totalImages <- map["totalImages"]
        distanceText <- map["distanceText"]
    }
}
